import datetime
import json
import re
from logging import getLogger
from typing import List, Tuple, TypeVar, Union

from dateparser import parse as _parse

try:
    from .TimeRange import TimeRange
except ImportError:
    from TimeRange import TimeRange

_T = TypeVar('_T')

base_date = datetime.datetime(2000, 1, 1)
DaysListType = Tuple[str, str, str, str, str, str, str, str]

log = getLogger(__name__)


def WeekOrTime(dt):
    if dt is None:
        return None
    if dt.date() != base_date.date():
        return dt.date().weekday() + 1
    return dt.time()


class Sep:
    days = ','
    times = '; '

    timerange = '-'  # разделитель диапазона времени
    dayrange = '-'  # разделитель диапазона дней

    range = '; '  # разделитель диапазонов
    minute = ':'
    daytime = ' '

    def update(self, d: dict):
        for k, v in d.items():
            try:
                self.__getattribute__(k)
            except AttributeError:
                log.warning(f'%s not in {self.__class__.__name__}' % k)
            else:
                self.__setattr__(k, v)


repl = (
    ('и до последнего клиента', ''),  # zoon.ru
    ('ежедневно', 'пн-вс'),
    ('everyday', 'пн-вс'),
    ('круглосуточно', '00:00-23:59'),
    ('24 часа', '00:00-23:59'),
    ('24:00', '23:59'),
    ('будни', 'пн-пт'),
    ('выходные', 'сб-вс'),

    ('пн:', 'пн '),
    ('вт:', 'вт '),
    ('ср:', 'ср '),
    ('чт:', 'чт '),
    ('пт:', 'пт '),
    ('сб:', 'сб '),
    ('вс:', 'вс ')
)


class Worktime(dict):
    def __str__(self):
        try:
            return json.dumps(self, default=lambda o: str(o), sort_keys=True)
        except Exception as e:
            log.error(e)
            return str()


def replace(s, r):
    for i in r:
        s = s.replace(*i)

    return s


def parser(text: List[str], languages=('ru', 'en')) -> Worktime or None:
    try:
        ret = _parser(text, languages)
    except Exception as e:
        raise Exception(f'{text}: {e}')
    else:
        log.debug(f'{text} -> {ret}')
        return ret


def process_single_line(text: list) -> list:
    time_re = r'\d{1,2}[:.]\d{2}'
    time_dia = '(?P<time%d>' + time_re + '-' + time_re + ')'
    days_dia = r'(?P<days%d>(, [а-я]{2}\.?)+) '
    re_days = re.compile(r'([а-я]{2})')
    days = {'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'}

    r = list()
    r.append('^' + time_dia % 0 + '$')
    r.append('^' + time_dia % 0 + days_dia % 0 + '- вых[\.:]?$')
    r.append('^' + time_dia % 0 + days_dia % 0 + time_dia % 1 + '$')
    r.append('^' + time_dia % 0 + days_dia % 0 + time_dia % 1 + days_dia % 1 + '- вых[\.:]?$')

    res = [re.compile(i).match(text[0]) for i in r]

    if res[0]:
        res = res[0].groupdict()
        text = [f'пн-вс ' + res["time0"]]
    elif res[1]:
        res = res[1].groupdict()
        c = days ^ set(re_days.findall(res['days0']))
        text = [', '.join(c) + ' ' + res['time0']]
    elif res[2]:
        res = res[2].groupdict()
        b = re_days.findall(res['days0'])
        c = days ^ set(b)
        text = [', '.join(c) + ' ' + res['time0'], ', '.join(b) + ' ' + res['time1']]
    elif res[3]:
        res = res[3].groupdict()
        b = re_days.findall(res['days0'])
        c = days ^ set(b + re_days.findall(res['days1']))
        text = [', '.join(c) + ' ' + res['time0'], ', '.join(b) + ' ' + res['time1']]

    # yp, spr fix
    elif ';' in text[0]:
        text = text[0].split(';')
    elif ',' in text[0]:
        t = [i.strip() for i in text[0].split(',')]
        # пн, вт, ср 9:00-10:00
        if not (days & set(t)):
            text = t

    return text


class TimeRanges(list):
    def append(self, obj: TimeRange):
        if obj in self:
            return
        for i in self:
            if i.is_intersection(obj):
                raise Exception(f'Time intersection[{i},{obj}]')
        super().append(obj)


def prepare_data(text: List[str]) -> List[str]:
    text = [i.lower() for i in text]
    text = [
        replace(
            i, (
                ('понедельник', 'пн'),
                ('вторник', 'вт'),
                ('среда', 'ср'),
                ('четверг', 'чт'),
                ('пятница', 'пт'),
                ('суббота', 'сб'),
                ('воскресенье', 'вс')
            )
        ) for i in text
    ]

    # moscowmap fix
    if text == ['круглосуточно']:
        text = ['ежедневно круглосуточно']

    # flump fix
    elif all([re.compile(r'\w{2} \d{2}:\d{2} \d{2}:\d{2}').findall(i) for i in text]):
        text = [re.sub(r'(\d{2}:\d{2}) (\d{2}:\d{2})', r'\1-\2', i) for i in text]

    elif len(text) == 1:
        text = process_single_line(text)

    # Исправляем точку как разделитель часов-минут
    items = [re.sub(r'(\d{1,2})\.(\d{2})', r'\1:\2', i) for i in text]
    # Приводим тире к одному стандарту, убираем пробелы вокруг
    items = [re.sub(r'\s*[—–−-]\s*', '-', replace(i, repl)) for i in items]
    # Часто встречающаяся конструкция c-до
    items = [re.sub(r'[сc] (\d{1,2}:\d{2}) до (\d{1,2}:\d{2})', r'\1-\2', i) for i in items]
    # Убираем точку после сокращения дня недели
    items = [re.sub(r'(\w{2})\.', r'\1', i) for i in items]
    # Убираем двоеточие между днями недели и временем
    items = [re.sub(r'(\w{2}): (\d{1,2}:)', r'\1 \2', i) for i in items]

    return items


def _parser(text: List[str], languages=('ru', 'en')) -> Union[Worktime, None]:
    if text is None or not len(text):
        return None
    if isinstance(text, str):
        return None
    start = list(text)
    items = prepare_data(text)

    out = Worktime()

    def wtp(x) -> Union[int, datetime.time]:
        return WeekOrTime(
            _parse(x, languages=languages, settings={'RELATIVE_BASE': base_date})
        )

    for item in items:
        words = [str(word) for word in item.split() if len(str(word)) > 1]
        days = list()
        time_ranges = TimeRanges()
        outd = Worktime()
        for num, word in enumerate(words):
            if '-' in word:
                r = [wtp(x) for x in word.split('-')]
                if len(r) == 2:
                    if all([type(x) == int for x in r]):  # daterange
                        if r[0] > r[1]:
                            r[1] += 7
                            r = list(range(r[0], r[1] + 1))
                            r = [((i - 1) % 7) + 1 for i in r]
                        else:
                            r = list(range(r[0], r[1] + 1))
                        days += r
                    elif all([type(x) is datetime.time for x in r]):  # timerange
                        time_ranges.append(TimeRange(*r))
                    elif type(r[0]) == int and r[1] is None:  # weekend
                        if r[0] in days:
                            days.remove(r[0])
                    else:
                        log.error(f'{start} parse error: {word} -> {r}')
                else:
                    for i in r:
                        days.append(i)
            else:  # single date or None
                r = wtp(word)
                if r == base_date.time():
                    r = 6
                if r is not None:
                    days.append(r)

        for r in (days or range(1, 8)):
            if time_ranges:
                outd.update({
                    str(r): [str(i) for i in sorted(time_ranges)]
                })
        out.update(outd)

    if not out:
        return None

    return out


def humanize(data: dict, *, days: DaysListType, separators: dict = None, overflow=()) -> str:
    if type(data) == str:
        data = json.loads(data)

    sep = Sep()
    if separators:
        sep.update(separators)

    times = data.values()
    unc_times = list()
    for i in times:
        if i not in unc_times:
            unc_times.append(i)

    def filter_value(dictionary, value):
        return dict(filter(lambda x: x[1] == value, dictionary.items()))

    ret = list()
    for time_ranges in unc_times:
        try:
            keys = [int(i) for i in sorted(list(filter_value(data, time_ranges)))]
        except:
            continue
        start, end = keys[0], keys[-1]
        r = list(range(int(start), int(end) + 1))
        if r == keys and len(r) > 1:
            keys = days[start] + sep.dayrange + days[end]
        else:
            keys = [days[i] for i in keys]
            keys = sep.days.join(keys)
        # соеденяем промежутки времени
        tm = [str(i).replace(':', sep.minute) for i in time_ranges]
        # создаем список str(дни), str(время)
        dt = [keys, sep.times.join(tm)]
        # добавляем в результат дни соедененные со временем
        ret.append(sep.daytime.join(dt))
    # соеденяем полученные диапазоны за промежутки дней
    rez = sep.range.join(ret)

    # ежедневно, круглосуточно
    if len(overflow) == 2:
        rez = rez.replace(f'{days[1]}{sep.dayrange}{days[7]}', overflow[0])
        rez = rez.replace(f'00{sep.minute}00{sep.timerange}23{sep.minute}59', overflow[1])

    return rez


if __name__ == '__main__':
    try:
        from .tests import tests
    except ImportError:
        from tests import tests

    format1 = ('ежед.', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс')
    format2 = ('ежедневно', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье')
    # a = parser(['вс–чт 9.00–00.00', 'пт, сб 9.00–6.00, 10:00-11:00'])
    # print(a)

    for n, i in enumerate(tests):
        print('===' + str(n) + '===')
        a = parser(i)
        print(i)
        print(a)
        print(humanize(a, days=format1))
        print(humanize(a, days=format2))
        print(humanize(a, days=format2, overflow=('ежедневно', 'круглосуточно')))
