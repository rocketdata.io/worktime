# used code from: https://github.com/thombashi/DateTimeRange

from datetime import datetime, time, timedelta
from typing import Union

import dateutil.parser
import dateutil.relativedelta as rdelta
from dateutil import parser


class TimeRange:
    NOT_A_TIME_STR = "NaT"
    __end_datetime = None
    __start_datetime = None

    def __init__(
            self,
            start_datetime=None,
            end_datetime=None,
            time_format="%H:%M"
    ):
        self.time_format = time_format
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime

        self.is_output_elapse = False
        self.separator = "-"

    def __repr__(self):
        text_list = [self.start_time_str, self.end_time_str]

        if self.is_output_elapse:
            suffix = f" (%s)" % (self.end_datetime - self.start_datetime)
        else:
            suffix = ""

        return self.separator.join(text_list) + suffix

    def __eq__(self, other):
        if not isinstance(other, TimeRange):
            return False

        return all(
            [self.start_datetime == other.start_datetime, self.end_datetime == other.end_datetime]
        )

    def __ne__(self, other):
        if not isinstance(other, TimeRange):
            return True

        return any(
            [self.start_datetime != other.start_datetime, self.end_datetime != other.end_datetime]
        )

    def __hash__(self):
        return (self.start_datetime, self.end_datetime).__hash__()

    def __add__(self, other):
        return TimeRange(self.start_datetime + other, self.end_datetime + other)

    def __iadd__(self, other):
        self.start_datetime = self.start_datetime + other
        self.end_datetime = self.end_datetime + other

        return self

    def __sub__(self, other):
        return TimeRange(self.start_datetime - other, self.end_datetime - other)

    def __isub__(self, other):
        self.start_datetime = self.start_datetime - other
        self.end_datetime = self.end_datetime - other

        return self

    def __contains__(self, x):
        if isinstance(x, TimeRange):
            return x.start_datetime >= self.start_datetime and x.end_datetime <= self.end_datetime

        try:
            value = dateutil.parser.parse(x)
        except (TypeError, AttributeError):
            value = x

        return self.start_datetime <= value <= self.end_datetime

    def __gt__(self, other: 'TimeRange'):
        if self.start_datetime != other.start_datetime:
            return self.start_datetime > other.start_datetime
        if self.end_datetime != other.end_datetime:
            return self.end_datetime > other.end_datetime
        return False

    @property
    def timedelta(self) -> timedelta:
        return self.end_datetime - self.start_datetime

    def is_set(self) -> bool:
        return all([self.start_datetime is not None, self.end_datetime is not None])

    def check_time_inversion(self) -> bool:
        if not self.is_set():
            # for python2/3 compatibility
            raise TypeError

        if self.start_datetime > self.end_datetime:
            return True
        return False

    def is_valid_timerange(self) -> bool:
        if self.check_time_inversion():
            return False

        return self.is_set()

    def is_intersection(self, x) -> bool:
        return self.intersection(x).is_set()

    @staticmethod
    def convert_datetime(value: Union[str, datetime]) -> Union[datetime, None]:
        if value is None:
            return None
        if type(value) == datetime:
            return value
        else:
            return parser.parse(str(value))

    @property
    def start_time_str(self) -> str:
        try:
            return self.start_datetime.strftime(self.time_format)
        except AttributeError:
            return self.NOT_A_TIME_STR

    @property
    def end_time_str(self) -> str:
        try:
            return self.end_datetime.strftime(self.time_format)
        except AttributeError:
            return self.NOT_A_TIME_STR

    @property
    def timedelta_second(self) -> int:
        return self.__get_timedelta_sec(self.timedelta)

    def set_time_range(self, start: datetime, end: datetime) -> 'TimeRange':
        self.start_datetime = start
        self.end_datetime = end
        return self

    @staticmethod
    def __compare_relativedelta(lhs: rdelta.relativedelta, rhs: rdelta.relativedelta):
        if lhs.years < rhs.years:
            return -1
        if lhs.years > rhs.years:
            return 1

        if lhs.months < rhs.months:
            return -1
        if lhs.months > rhs.months:
            return 1

        if lhs.days < rhs.days:
            return -1
        if lhs.days > rhs.days:
            return 1

        if lhs.hours < rhs.hours:
            return -1
        if lhs.hours > rhs.hours:
            return 1

        if lhs.minutes < rhs.minutes:
            return -1
        if lhs.minutes > rhs.minutes:
            return 1

        if lhs.seconds < rhs.seconds:
            return -1
        if lhs.seconds > rhs.seconds:
            return 1

        if lhs.microseconds < rhs.microseconds:
            return -1
        if lhs.microseconds > rhs.microseconds:
            return 1

        return 0

    def __compare_timedelta(self, lhs, seconds: int):
        try:
            rhs = timedelta(seconds=seconds)

            if lhs < rhs:
                return -1
            if lhs > rhs:
                return 1

            return 0
        except TypeError:
            return self.__compare_relativedelta(
                lhs.normalized(), rdelta.relativedelta(seconds=seconds)
            )

    def range(self, step):
        if self.__compare_timedelta(step, 0) == 0:
            raise ValueError("step must be not zero")

        if self.check_time_inversion():
            if self.__compare_timedelta(step, seconds=0) > 0:
                raise ValueError(f"invalid step: expect less than 0, actual={step}")
        else:
            if self.__compare_timedelta(step, seconds=0) < 0:
                raise ValueError(f"invalid step: expect greater than 0, actual={step}")

        current_datetime = self.start_datetime
        while current_datetime <= self.end_datetime:
            yield current_datetime
            current_datetime = current_datetime + step

    def intersection(self, x: 'TimeRange') -> 'TimeRange':
        if any([x.start_datetime in self, self.start_datetime in x]):
            start_datetime = max(self.start_datetime, x.start_datetime)
            end_datetime = min(self.end_datetime, x.end_datetime)
        else:
            start_datetime = None
            end_datetime = None

        return TimeRange(
            start_datetime=start_datetime,
            end_datetime=end_datetime,
            time_format=self.time_format,
        )

    def encompass(self, x: 'TimeRange') -> 'TimeRange':
        return TimeRange(
            start_datetime=min(self.start_datetime, x.start_datetime),
            end_datetime=max(self.end_datetime, x.end_datetime),
            time_format=self.time_format,
        )

    def truncate(self, percentage) -> 'TimeRange':
        if self.check_time_inversion():
            raise Exception('Time inverted')

        if percentage < 0:
            raise ValueError("discard_percent must be greater or equal to zero: " + str(percentage))

        if percentage == 0:
            return self

        discard_time = self.timedelta // int(100) * int(percentage / 2)

        self.__start_datetime += discard_time
        self.__end_datetime -= discard_time
        return self

    @staticmethod
    def __get_timedelta_sec(dt: timedelta) -> int:
        return int(dt.days * 60 ** 2 * 24 + dt.seconds + dt.microseconds / 1000 ** 2)

    @property
    def start_datetime(self):
        return self.__start_datetime

    @start_datetime.setter
    def start_datetime(self, value: Union[str, datetime]):
        value = self.convert_datetime(value)

        if self.end_datetime and value > self.end_datetime:
            value = value - timedelta(days=1)

        self.__start_datetime = value

    @property
    def end_datetime(self) -> datetime:
        return self.__end_datetime

    @end_datetime.setter
    def end_datetime(self, value: Union[str, datetime]):
        value = self.convert_datetime(value)

        if value and value.time() == time(0, 0):
            value = datetime.now().replace(hour=23, minute=59)

        if self.start_datetime and value < self.start_datetime:
            value = value + timedelta(days=1)

        self.__end_datetime = value


if __name__ == '__main__':
    a = TimeRange('18:00', '10:00')
    a.is_output_elapse = True
    print(a.timedelta_second)
    print(a)
