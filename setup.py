#!/bin/python

from setuptools import setup, find_packages

PACKAGE = "worktime"
NAME = "worktime"
DESCRIPTION = "Worktime parser library"
AUTHOR = "Myrik"
AUTHOR_EMAIL = "myrik260138@tut.by"
URL = ""
VERSION = "0.0.1"

with open("README.md") as f:
    LONG_DESCRIPTION = f.read()

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    license="BSD",
    url=URL,
    packages=find_packages(exclude=["tests.*", "tests"]),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8"
    ],
    install_requires=[
        "dateparser",
        "python-dateutil"
    ],
    zip_safe=True,
)
